// MFCConst.cpp : コンソール アプリケーションのエントリ ポイントを定義します。
//

#include "stdafx.h"

struct Param
{
	int value;

	Param()
		: value(0)
	{}
};

void func1(Param* pParam)
{
	pParam->value = 1;
	pParam = NULL; // コンパイルエラーにならない
}

void func2(Param* const pParam)
{
	pParam->value = 2;
	//pParam = NULL; // コンパイルエラー
}

void func3(Param& param)
{
	param.value = 3;
	//param = NULL; // コンパイルエラー
}

int _tmain(int argc, _TCHAR* argv[])
{
	Param param;
	Param* pParam = &param;
	
	func1(pParam);
	// func1(NULL); 実行時エラー

	func2(pParam);
	// func2(NULL); 実行時エラー

	func3(param);
	// func3(NULL); コンパイルエラー

	return 0;
}

